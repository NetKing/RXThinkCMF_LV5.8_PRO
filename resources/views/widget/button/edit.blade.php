@if (in_array($funcAuth, $permissionList))
<a class="layui-btn layui-btn-xs btnEdit" lay-event="edit" title="{{$funcTitle}}"><i class="layui-icon">&#xe642;</i>{{$funcTitle}}</a>
@endif
